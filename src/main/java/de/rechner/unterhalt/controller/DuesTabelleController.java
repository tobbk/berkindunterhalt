package de.rechner.unterhalt.controller;

import java.util.ArrayList;

import de.rechner.unterhalt.model.DuesTabelle;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

public class DuesTabelleController {

	@FXML private AnchorPane duesTabelleView;
	private DuesTabelle duesTabelle;

	private MainController mainController;

	public void injectMainController(MainController mainController) {
		this.mainController = mainController;
	}

	public int getKinderGeld (int einkommen, int altStufe) {

		int kg = 0;

		ArrayList<Integer> einkommenList = duesTabelle.getNettoeinkommen();

		for (int i = 0; i < einkommenList.size(); i++) {

			if (einkommen <= einkommenList.get(i) || (einkommen > einkommenList.get(i) && i == einkommenList.size()-1)) {

				switch(altStufe) {
				case 1: kg = duesTabelle.getKgStufe1().get(i);
				break;
				case 2: kg = duesTabelle.getKgStufe2().get(i);
				break;
				case 3:kg = duesTabelle.getKgStufe3().get(i);
				break;
				}
				return kg;
			}

		}

		return kg;
	}

	public AnchorPane getDuesTabView() {
		return duesTabelleView;
	}

}
