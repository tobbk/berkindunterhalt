package de.rechner.unterhalt.controller;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class MainController {
	
	//Menu
	@FXML 
	private MenuController menuController;
	
	//Allgemeine Info
	@FXML 
	private AllgemeinController allgInfoController;
	
	//Duesseldorfer Tabelle
	@FXML 
	private DuesTabelleController duesTabController;
	
	//Mangefallberechnung
	@FXML 
	private MangelfallBerController mangefallController;
	
	//Settings
	@FXML 
	private SettingsController settingsController;
	
	private final BooleanProperty menuShowing = new SimpleBooleanProperty(this, "menuShowing", false);
	

	@FXML 
	private void initialize() {
		
		menuController = new MenuController();
		allgInfoController = new AllgemeinController();
		duesTabController = new DuesTabelleController();
		mangefallController = new MangelfallBerController();
		
		menuController.injectMainController(this);
//		allgInfoController.injectMainController(this);
//		duesTabController.injectMainController(this);
//		mangefallController.injectMainController(this);
//		settingsController.injectMainController(this);
	}

	public AnchorPane getAllgInfoAP() {
		return allgInfoController.getAllgInfoView();
	}
	
	public AnchorPane getDuesTabAP() {
		return duesTabController.getDuesTabView();
	}
	
	public AnchorPane getMangelFallBerechnungAP() {
		return mangefallController.getMangelFallBerechnungView();
	}
	
	public AnchorPane getSettingsAP() {
		return settingsController.getSettingsView();
	}
	
	
}
