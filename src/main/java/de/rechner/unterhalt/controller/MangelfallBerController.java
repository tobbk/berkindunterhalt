package de.rechner.unterhalt.controller;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class MangelfallBerController {
	
	@FXML private AnchorPane mangelView;
	
	private MainController mainController;
	
	public void injectMainController(MainController mainController) {
	 this.mainController = mainController;
	}
	

	public AnchorPane getMangelFallBerechnungView() {
		return mangelView;
	}

}
