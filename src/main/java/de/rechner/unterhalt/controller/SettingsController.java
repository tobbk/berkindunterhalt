package de.rechner.unterhalt.controller;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class SettingsController {

	@FXML private AnchorPane settingsView;

	private MainController mainController;

	public void injectMainController(MainController mainController) {
		this.mainController = mainController;
	}


	public AnchorPane getSettingsView() {
		return settingsView;
	}

}
