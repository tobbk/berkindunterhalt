package de.rechner.unterhalt.controller;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class MenuController {

	@FXML private AnchorPane menuView;
	
	@FXML private JFXButton allgInfo;
	@FXML private JFXButton mangelBerechnung;
	@FXML private JFXButton dusTabelle;
	
	@FXML private AnchorPane allgInfoView;
	@FXML private AnchorPane mangelView;
	@FXML private AnchorPane duesTabelleView;
	
	private MainController mainController;
	
	public void injectMainController(MainController mainController) {
	 this.mainController = mainController;
	}
	
	@FXML
	public void openAllgInfo() {
			
		mainController.getAllgInfoAP().setVisible(true);
		mainController.getMangelFallBerechnungAP().setVisible(false);
		mainController.getDuesTabAP().setVisible(false);
		
	}

	@FXML
	public void openMangelBerechnung() {
		
		mainController.getAllgInfoAP().setVisible(false);
		mainController.getMangelFallBerechnungAP().setVisible(true);
		mainController.getDuesTabAP().setVisible(false);
		
	}
	
	@FXML
	public void openDusTabelle() {
		
		mainController.getAllgInfoAP().setVisible(false);
		mainController.getMangelFallBerechnungAP().setVisible(false);
		mainController.getDuesTabAP().setVisible(true);
		
	}
	
}
