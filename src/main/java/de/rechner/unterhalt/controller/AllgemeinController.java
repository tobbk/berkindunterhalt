package de.rechner.unterhalt.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;

import de.rechner.unterhalt.model.AllgInfo;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class AllgemeinController
{
	private static final Logger log = LoggerFactory.getLogger(AllgemeinController.class);

	private static AllgemeinController allgController;
	
	@FXML private AnchorPane allgInfoView;

	@FXML private JFXDatePicker antragstellung;
	@FXML private JFXTextField stellenzeichen;

	@FXML private JFXTextField vname;
	@FXML private JFXTextField name;
	@FXML private JFXDatePicker gebDatum;
	@FXML private JFXTextField einkommen;
	@FXML private JFXTextField sonderzuw;
	@FXML private JFXTextField sonderzuwGrund;
	@FXML private JFXTextField verLstg;
	@FXML private JFXTextField mtlBelastung;
	@FXML private JFXTextField mtlBelastungGrund;
	@FXML private JFXTextField sonstAbzug;
	@FXML private JFXTextField sonstAbzugGrund;
	@FXML private JFXTextField sonstZuschlag;
	@FXML private JFXTextField sonstZuschlagGrund;

	@FXML private JFXTextField alterst1;
	@FXML private JFXTextField alterst2;
	@FXML private JFXTextField alterst3;

	@FXML private JFXTextField zuLeistUnterhalt;

	@FXML private Label alter;

	private AllgInfo allgInfo;
	private DuesTabelleController duesTabelleController;


	public void fillModelValues() {

		allgInfo.setStellenzeichen(stellenzeichen.getText());
		allgInfo.setVname(vname.getText());
		allgInfo.setName(name.getText());
		allgInfo.setEinkommen(Integer.parseInt(einkommen.getText() == null ? "0" : einkommen.getText()));
		allgInfo.setSonderzuw(Integer.parseInt(sonderzuw.getText() == null ? "0" : sonderzuw.getText()));
		allgInfo.setSonderzuwGrund(sonderzuwGrund.getText() );
		allgInfo.setVerLstg(Integer.parseInt(verLstg.getText() == null ? "0" : verLstg.getText()));
		allgInfo.setMtlBelastung(Integer.parseInt(mtlBelastung.getText() == null ? "0" : mtlBelastung.getText()));
		allgInfo.setMtlBelastungGrund(mtlBelastungGrund.getText());
		allgInfo.setSonstAbzug(Integer.parseInt(sonstAbzug.getText() == null ? "0" : sonstAbzug.getText()));
		allgInfo.setSonstAbzugGrund(sonstAbzugGrund.getText());
		allgInfo.setSonstZuschlag(Integer.parseInt(sonstZuschlag.getText() == null ? "0" : sonstZuschlag.getText()));
		allgInfo.setAlterst1(Integer.parseInt(alterst1.getText() == null ? "0" : alterst1.getText()));
		allgInfo.setAlterst2(Integer.parseInt(alterst2.getText() == null ? "0" : alterst2.getText()));
		allgInfo.setAlterst3(Integer.parseInt(alterst3.getText() == null ? "0" : alterst3.getText()));

		calculateAliment();

	}

	public void fillModelDateValues() {

		try {
			if ((gebDatum.toString() != null) && (antragstellung.toString() != null )) {
				allgInfo.setGebDatum(new SimpleDateFormat("dd/MM/yyyy").parse(gebDatum.toString()));
				allgInfo.setAntragstellung((new SimpleDateFormat("dd/MM/yyyy").parse(antragstellung.toString())));
			}			
		} catch (Exception e) {
			log.error("Bei der Konvertierung beim Datum ist ein Fehler aufgetreten", gebDatum.toString(), antragstellung.toString());
		}

	}

	public void calculateAge() {

		Calendar antrag = getCalendar(allgInfo.getAntragstellung());
		Calendar gebDatum = getCalendar(allgInfo.getGebDatum());

		int diff = gebDatum.get(Calendar.YEAR) - antrag.get(Calendar.MONTH);

		if (antrag.get(Calendar.MONTH) > gebDatum.get(Calendar.MONTH) || 
				(antrag.get(Calendar.MONTH) == gebDatum.get(Calendar.MONTH) && antrag.get(Calendar.DATE) > gebDatum.get(Calendar.DATE))) {
			diff--;
		}

		alter.setText(Integer.toString(diff));

	}


	public static Calendar getCalendar(Date date) {
		Calendar cal = Calendar.getInstance(Locale.GERMANY);
		cal.setTime(date);
		return cal;
	}


	public void calculateAliment() {

		fillModelValues();

		Integer verfBetrag = allgInfo.getEinkommen() + allgInfo.getSonstZuschlag() - allgInfo.getBedarfsKontrollBetrag() - allgInfo.getSonderzuw() - allgInfo.getMtlBelastung() - allgInfo.getSonstAbzug();

		if (verfBetrag < duesTabelleController.getKinderGeld(allgInfo.getEinkommen(), allgInfo.getAlter())) {

			zuLeistUnterhalt.setText(Integer.toString(verfBetrag));

			//TODO
			//Button zum drucken wird auf visible gesetzt
		}else {
			//TODO
			// Button zur Mangelfallberechnung wird auf visible gesetzt
		}

	}

	public void newStellenzeichen() {

		AllgInfo allgInfo = AllgInfo.getInstance();

		if ((!allgInfo.getStellenzeichen().isEmpty()) && (stellenzeichen != null) && (allgInfo.getStellenzeichen() != stellenzeichen.getText())) {

			allgInfo.reset();
			resetUI();

		}
	}
	public void resetUI() {

		antragstellung = null;
		vname = null;
		name = null;
		gebDatum = null;
		einkommen = null;
		sonderzuw = null;
		sonderzuwGrund = null;
		verLstg = null;
		mtlBelastung = null;
		mtlBelastungGrund = null;
		sonstAbzug = null;
		sonstAbzugGrund = null;
		sonstZuschlag = null;
		sonstZuschlagGrund = null;
		alterst1 = null;
		alterst2 = null;
		alterst3 = null;
		zuLeistUnterhalt = null;

	}

	public AnchorPane getAllgInfoView() {
		return allgInfoView;
	}

}
