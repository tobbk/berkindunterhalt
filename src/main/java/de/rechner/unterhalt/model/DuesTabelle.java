package de.rechner.unterhalt.model;

import java.util.ArrayList;
import java.util.HashMap;

public class DuesTabelle {
	    
    private static DuesTabelle duesTabelle;
	
	private ArrayList<Integer> nettoeinkommen;
	private HashMap<Integer, Integer> kgStufe1;
	private HashMap<Integer, Integer> kgStufe2;
	private HashMap<Integer, Integer> kgStufe3;
	
	
    private DuesTabelle() {
    	
    }
    
    //Singelton Pattern
    public static DuesTabelle getInstance() {
    	
    	if (duesTabelle == null) {
    		duesTabelle = new DuesTabelle();
    	}
    	
    	return duesTabelle;
    }
    
	
	public ArrayList<Integer> getNettoeinkommen() {
		return nettoeinkommen;
	}
	
	public void setNettoeinkommen(ArrayList<Integer> nettoeinkommen) {
		this.nettoeinkommen = nettoeinkommen;
	}
	
	public HashMap<Integer, Integer> getKgStufe1() {
		return kgStufe1;
	}
	
	public void setKgStufe1(HashMap<Integer, Integer> kgStufe1) {
		this.kgStufe1 = kgStufe1;
	}
	
	public HashMap<Integer, Integer> getKgStufe2() {
		return kgStufe2;
	}
	
	public void setKgStufe2(HashMap<Integer, Integer> kgStufe2) {
		this.kgStufe2 = kgStufe2;
	}
	
	public HashMap<Integer, Integer> getKgStufe3() {
		return kgStufe3;
	}
	
	public void setKgStufe3(HashMap<Integer, Integer> kgStufe3) {
		this.kgStufe3 = kgStufe3;
	}
	
}
