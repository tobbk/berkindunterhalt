package de.rechner.unterhalt.model;

import java.util.Date;

public class AllgInfo {
	
    private Date antragstellung;
    private String stellenzeichen;
	private String vname;
	private String name;
	private Date gebDatum;
	private Integer alter;
    private Integer einkommen;
    private Integer sonderzuw;
    private String sonderzuwGrund;
    private Integer verLstg;
    private Integer mtlBelastung;
    private String mtlBelastungGrund;
    private Integer sonstAbzug;
    private String sonstAbzugGrund;
    private Integer sonstZuschlag;
    private String sonstZuschlagGrund;
    
    private Integer alterst1;
    private Integer alterst2;
    private Integer alterst3;
    
    private Integer zuLeistUnterhalt;
    private Integer bedarfsKontrollBetrag = 1080;
    
    private static AllgInfo allgInfo;
    
    private AllgInfo() {
    	
    }
    
    //Singelton Pattern
    public static AllgInfo getInstance() {
    	
    	if (allgInfo == null) {
    		allgInfo = new AllgInfo();
    	}
    	
    	return allgInfo;
    }
    
    public void reset() {
    	
    	antragstellung = null;
    	stellenzeichen = null;
    	vname = null;
    	name = null;
    	gebDatum = null;
    	einkommen = null;
    	sonderzuw = null;
    	sonderzuwGrund = null;
    	verLstg = null;
    	mtlBelastung = null;
    	mtlBelastungGrund = null;
    	sonstAbzug = null;
    	sonstAbzugGrund = null;
    	sonstZuschlag = null;
    	sonstZuschlagGrund = null;
    	alterst1 = null;
    	alterst2 = null;
    	alterst3 = null;
    	zuLeistUnterhalt = null;
    	
    }

	public Date getAntragstellung() {
		return antragstellung;
	}

	public void setAntragstellung(Date antragstellung) {
		this.antragstellung = antragstellung;
	}

	public String getStellenzeichen() {
		return stellenzeichen;
	}

	public void setStellenzeichen(String stellenzeichen) {
		this.stellenzeichen = stellenzeichen;
	}

	public String getVname() {
		return vname;
	}

	public void setVname(String vname) {
		this.vname = vname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getGebDatum() {
		return gebDatum;
	}

	public void setGebDatum(Date gebDatum) {
		this.gebDatum = gebDatum;
	}
	
	public Integer getAlter() {
		return alter;
	}

	public void setAlter(Integer alter) {
		this.alter = alter;
	}

	public Integer getEinkommen() {
		return einkommen;
	}

	public void setEinkommen(Integer einkommen) {
		this.einkommen = einkommen;
	}

	public Integer getSonderzuw() {
		return sonderzuw;
	}

	public void setSonderzuw(Integer sonderzuw) {
		this.sonderzuw = sonderzuw;
	}

	public String getSonderzuwGrund() {
		return sonderzuwGrund;
	}

	public void setSonderzuwGrund(String sonderzuwGrund) {
		this.sonderzuwGrund = sonderzuwGrund;
	}

	public Integer getVerLstg() {
		return verLstg;
	}

	public void setVerLstg(Integer verLstg) {
		this.verLstg = verLstg;
	}

	public Integer getMtlBelastung() {
		return mtlBelastung;
	}

	public void setMtlBelastung(Integer mtlBelastung) {
		this.mtlBelastung = mtlBelastung;
	}

	public String getMtlBelastungGrund() {
		return mtlBelastungGrund;
	}

	public void setMtlBelastungGrund(String mtlBelastungGrund) {
		this.mtlBelastungGrund = mtlBelastungGrund;
	}

	public Integer getSonstAbzug() {
		return sonstAbzug;
	}

	public void setSonstAbzug(Integer sonstAbzug) {
		this.sonstAbzug = sonstAbzug;
	}

	public String getSonstAbzugGrund() {
		return sonstAbzugGrund;
	}

	public void setSonstAbzugGrund(String sonstAbzugGrund) {
		this.sonstAbzugGrund = sonstAbzugGrund;
	}

	public Integer getSonstZuschlag() {
		return sonstZuschlag;
	}

	public void setSonstZuschlag(Integer sonstZuschlag) {
		this.sonstZuschlag = sonstZuschlag;
	}

	public String getSonstZuschlagGrund() {
		return sonstZuschlagGrund;
	}

	public void setSonstZuschlagGrund(String sonstZuschlagGrund) {
		this.sonstZuschlagGrund = sonstZuschlagGrund;
	}

	public Integer getAlterst1() {
		return alterst1;
	}

	public void setAlterst1(Integer alterst1) {
		this.alterst1 = alterst1;
	}

	public Integer getAlterst2() {
		return alterst2;
	}

	public void setAlterst2(Integer alterst2) {
		this.alterst2 = alterst2;
	}

	public Integer getAlterst3() {
		return alterst3;
	}

	public void setAlterst3(Integer alterst3) {
		this.alterst3 = alterst3;
	}

	public Integer getZuLeistUnterhalt() {
		return zuLeistUnterhalt;
	}

	public void setZuLeistUnterhalt(Integer zuLeistUnterhalt) {
		this.zuLeistUnterhalt = zuLeistUnterhalt;
	}

	public Integer getBedarfsKontrollBetrag() {
		return bedarfsKontrollBetrag;
	}

	public void setBedarfsKontrollBetrag(Integer bedarfsKontrollBetrag) {
		this.bedarfsKontrollBetrag = bedarfsKontrollBetrag;
	}   

}
